package server.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Database {
    private static Connection connection;

    public static void connect(String database, String user, String password, String port) {
        try {
            connection = DriverManager.getConnection(("jdbc:mysql://localhost:" + port + "/" +
                    database + "?serverTimezone=UTC"), user, password);
        } catch (SQLException sqlexc) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            sqlexc.printStackTrace();
        }
    }

    public static boolean addClient(String name, String surname, String login, String password, String email) {
        String insertClient = "INSERT INTO cost.users (name, surname, login, password, email) VALUES (?, ?, ?, ?, ?)";

        try {
            PreparedStatement preparedStatementClient = connection.prepareStatement(insertClient);

            preparedStatementClient.setString(1, name);
            preparedStatementClient.setString(2, surname);
            preparedStatementClient.setString(3, login);
            preparedStatementClient.setString(4, password);
            preparedStatementClient.setString(5, email);
            preparedStatementClient.executeUpdate();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public static boolean addAdmin(String login, String password) {
        String insertAdmin = "INSERT INTO cost.admin (login, password) VALUES (?, ?)";

        try {
            PreparedStatement preparedStatementClient = connection.prepareStatement(insertAdmin);

            preparedStatementClient.setString(1, login);
            preparedStatementClient.setString(2, password);
            preparedStatementClient.executeUpdate();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public static boolean login(String login, String password) {
        ResultSet resultSet;

        String select = "SELECT * FROM users WHERE login=? and password=?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(select);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            System.out.println("login func exception");
            e.printStackTrace();
        }
        return true;
    }

    public static boolean loginAdm(String login, String password) {
        ResultSet resultSet;

        String select = "SELECT * FROM admin WHERE login=? and password=?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(select);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            System.out.println("login func exception");
            e.printStackTrace();
        }
        return true;
    }

    public static boolean dellAdmin(String login) {

        String delete = "DELETE FROM cost.admin WHERE login=? ";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(delete);
            preparedStatement.setString(1, login);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public static ArrayList<String> getUsers() {
        ResultSet resultSet;
        ArrayList<String> usersList = new ArrayList<>(0);

        String select = "SELECT * FROM cost.users";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(select);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                StringBuilder info = new StringBuilder();
                info.append(resultSet.getString("name")).append(" ").
                        append(resultSet.getString("surname")).append(" ").
                        append(resultSet.getString("login")).append(" ").
                        append(resultSet.getString("email")).append(" ").
                        append(resultSet.getString("password"));
                usersList.add(info.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usersList;
    }

    public static ArrayList<String> getAdmin() {
        ResultSet resultSet;
        ArrayList<String> admin = new ArrayList<>(0);

        String select = "SELECT * FROM admin";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(select);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                admin.add(resultSet.getString("login"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return admin;
    }

    public static boolean addCountry(String country) {
        String insertCountry = "INSERT INTO cost.country (country) VALUES (?)";
        try {
            PreparedStatement preparedStatementClient = connection.prepareStatement(insertCountry);
            preparedStatementClient.setString(1, country);
            preparedStatementClient.executeUpdate();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public static ArrayList<String> getCountry() {
        ResultSet resultSet;
        ArrayList<String> country = new ArrayList<>(0);

        String select = "SELECT * FROM cost.country";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(select);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                country.add(resultSet.getString("country"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return country;
    }

    public static boolean delCountry(String country) {

        String delete = "DELETE FROM cost.country WHERE country=? ";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(delete);
            preparedStatement.setString(1, country);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public static boolean addYear(String year) {
        String insertCountry = "INSERT INTO cost.year (year) VALUES (?)";
        try {
            PreparedStatement preparedStatementClient = connection.prepareStatement(insertCountry);
            preparedStatementClient.setString(1, year);
            preparedStatementClient.executeUpdate();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public static ArrayList<String> getYear() {
        ResultSet resultSet;
        ArrayList<String> year = new ArrayList<>(0);

        String select = "SELECT * FROM cost.year";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(select);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                year.add(resultSet.getString("year"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return year;
    }

    public static boolean delYear(String year) {

        String delete = "DELETE FROM cost.year WHERE year=? ";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(delete);
            preparedStatement.setString(1, year);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public static boolean addProduct(String name) {
        String insertCountry = "INSERT INTO cost.products (name) VALUES (?)";
        try {
            PreparedStatement preparedStatementClient = connection.prepareStatement(insertCountry);
            preparedStatementClient.setString(1, name);
            preparedStatementClient.executeUpdate();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public static ArrayList<String> getProducts() {
        ResultSet resultSet;
        ArrayList<String> product = new ArrayList<>(0);

        String select = "SELECT * FROM cost.products";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(select);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                product.add(resultSet.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return product;
    }

    public static boolean delProduct(String name) {

        String delete = "DELETE FROM cost.products WHERE name=? ";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(delete);
            preparedStatement.setString(1, name);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public static ArrayList<String> getCost() {
        ResultSet resultSet;
        ArrayList<String> infoList = new ArrayList<>(0);

        String select = "SELECT * FROM cost.product";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(select);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                StringBuilder info = new StringBuilder();
                info.append(resultSet.getInt("id_product")).append(" ").
                        append(resultSet.getString("name")).append(" ").
                        append(resultSet.getString("country")).append(" ").
                        append(resultSet.getInt("year")).append(" ").
                        append(resultSet.getDouble("energy")).append(" ").
                        append(resultSet.getDouble("salary")).append(" ").
                        append(resultSet.getDouble("materialcost")).append(" ").
                        append(resultSet.getDouble("anothercosts")).append(" ").
                        append(resultSet.getDouble("cost"));
                infoList.add(info.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return infoList;
    }

    public static boolean addCost(String cost) {
        String[] infoDetails = cost.split(" ", 9);
        String insert = "INSERT INTO cost.product (salary, materialcost, cost, anothercosts, energy, country, year, name)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setString(1, infoDetails[1]);
            preparedStatement.setString(2, infoDetails[2]);
            preparedStatement.setString(3, infoDetails[3]);
            preparedStatement.setString(4, infoDetails[4]);
            preparedStatement.setString(5, infoDetails[5]);
            preparedStatement.setString(6, infoDetails[6]);
            preparedStatement.setString(7, infoDetails[7]);
            preparedStatement.setString(8, infoDetails[8]);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static ArrayList<String> getUserCost() {
        ResultSet resultSet;
        ArrayList<String> infoList = new ArrayList<>(0);

        String select = "SELECT * FROM cost.product";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(select);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                StringBuilder info = new StringBuilder();
                info.append(resultSet.getString("name")).append(" ").
                        append(resultSet.getString("country")).append(" ").
                        append(resultSet.getInt("year")).append(" ").
                        append(resultSet.getDouble("energy")).append(" ").
                        append(resultSet.getDouble("salary")).append(" ").
                        append(resultSet.getDouble("materialcost")).append(" ").
                        append(resultSet.getDouble("anothercosts")).append(" ").
                        append(resultSet.getDouble("cost"));
                infoList.add(info.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return infoList;
    }

}