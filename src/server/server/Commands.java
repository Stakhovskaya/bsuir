package server.server;

import server.Database.Database;

public class Commands {

    public static Object split(String command) {
        String[] commandNumber = command.split(" ", 2);
        String[] commands;
        Object result = true;
        switch (commandNumber[0]) {
            case "addAdmin":
                commands = command.split(" ", 5);
                result = Database.addAdmin(commands[1], commands[2]);
                break;
            case "addclient":
                commands = command.split(" ", 6);
                result = Database.addClient(commands[1], commands[2], commands[3], commands[4], commands[5]);
                break;
            case "login":
                commands = command.split(" ", 3);
                result = Database.login(commands[1], commands[2]);
                break;
            case "loginAdm":
                commands = command.split(" ", 3);
                result = Database.loginAdm(commands[1], commands[2]);
                break;
            case "dellAdmin":
                commands = command.split(" ", 2);
                result = Database.dellAdmin(commands[1]);
                break;
            case "getUsers":
                result = Database.getUsers();
                break;
            case "getAdmin":
                result = Database.getAdmin();
                break;
                //Country commands
            case "addCountry":
                commands = command.split(" ", 2);
                result = Database.addCountry(commands[1]);
                break;
            case "getCountry":
                result = Database.getCountry();
                break;
            case "delCountry":
                commands = command.split(" ", 2);
                result = Database.delCountry(commands[1]);
                break;
            //Year commands
            case "addYear":
                commands = command.split(" ", 2);
                result = Database.addYear(commands[1]);
                break;
            case "getYear":
                result = Database.getYear();
                break;
            case "delYear":
                commands = command.split(" ", 2);
                result = Database.delYear(commands[1]);
                break;
            //Product commands
            case "addProduct":
                commands = command.split(" ", 2);
                result = Database.addProduct(commands[1]);
                break;
            case "getProducts":
                result = Database.getProducts();
                break;
            case "delProduct":
                commands = command.split(" ", 2);
                result = Database.delProduct(commands[1]);
                break;
            //Cost commands
            case "getCost":
                result = Database.getCost();
                break;
            case "addCost":
                result = Database.addCost(command);
                break;
            case "getUserCost":
                result = Database.getUserCost();
                break;
        }
        return result;
    }
}
