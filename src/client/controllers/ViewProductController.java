package client.controllers;

import client.entityClass.Products;
import client.sample.Client;
import client.sample.ClientInstance;
import client.sceneLoaders.SceneLoaderInstance;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

import java.util.ArrayList;

public class ViewProductController {

    @FXML
    private AnchorPane productsTable;

    @FXML
    private TableView<Products> productTable;

    @FXML
    private TableColumn<Products, String> productColumn;

    @FXML
    private Button backButton;

    public void initialize() {
        fillTableView();
    }

    public void fillTableView() {
        Client client = ClientInstance.INSTANCE.getInstance();
        client.connect();
        ClientInstance.INSTANCE.getInstance().send("getProducts");
        ArrayList<String> list = ClientInstance.INSTANCE.getInstance().receiveResultList();
        ObservableList<Products> products = FXCollections.observableArrayList();
        for (int i = 0; i < list.size(); i++) {
            Products product = new Products();
            product.setName(list.get(i));
            products.add(product);
        }
        productColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        productTable.setItems(products);
    }

    @FXML
    void back() {
        backButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("adminMenu", "");
    }
}

