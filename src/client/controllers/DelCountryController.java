package client.controllers;

import client.sample.Alert;
import client.sample.ClientInstance;
import client.sceneLoaders.SceneLoaderInstance;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class DelCountryController {

    @FXML
    private TextField countryField;

    @FXML
    private Button backButton;

    public void del() {
        if (countryField.getText().equals("")) {
            Alert.display("Вы не ввели название страны!");
            return;
        }
        ClientInstance.INSTANCE.getInstance().send(("delCountry " + countryField.getText()));
        if (ClientInstance.INSTANCE.getInstance().receiveResultBool()) {
            Alert.display("Данные удалены успешно!");
        }
    }

    @FXML
    void back() {
        backButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("adminMenu", "");
    }
}

