package client.controllers;

import client.sample.Alert;
import client.sample.ClientInstance;
import client.sceneLoaders.SceneLoaderInstance;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class AddYearController {

    @FXML
    private TextField yearField;

    @FXML
    private Button backButton;

    public void addYear() {
        if (yearField.getText().equals("")) {
            Alert.display("Вы не ввели год выпуска продукции!");
            return;
        }
        ClientInstance.INSTANCE.getInstance().send(("addYear " + yearField.getText()));
        if (ClientInstance.INSTANCE.getInstance().receiveResultBool()) {
            Alert.display("Год выпуска добавлен успешно!");
        }
    }

    @FXML
    void back() {
        backButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("adminMenu2", "");
    }
}
