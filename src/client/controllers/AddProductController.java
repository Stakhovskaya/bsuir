package client.controllers;

import client.sample.Alert;
import client.sample.ClientInstance;
import client.sceneLoaders.SceneLoaderInstance;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class AddProductController {

    @FXML
    private TextField productField;

    @FXML
    private Button backButton;

    public void addProduct() {
        if (productField.getText().equals("")) {
            Alert.display("Вы не ввели название продукта!");
            return;
        }
        ClientInstance.INSTANCE.getInstance().send(("addProduct " + productField.getText()));
        if (ClientInstance.INSTANCE.getInstance().receiveResultBool()) {
            Alert.display("Продукт добавлен успешно!");
        }
    }

    @FXML
    void back() {
        backButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("adminMenu", "");
    }
}

