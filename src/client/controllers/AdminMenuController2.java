package client.controllers;

import client.sceneLoaders.SceneLoaderInstance;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class AdminMenuController2 {

    @FXML
    private Button backButton;

    @FXML
    private Button viewCost;

    @FXML
    private Button addCost;

    @FXML
    private Button dellCost;

    @FXML
    private Button viewYear;

    @FXML
    private Button addYear;

    @FXML
    private Button dellYear;
    @FXML
    private Button viewUsers;

    @FXML
    void  viewCostB() {
        viewCost.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("viewCost", "");
    }

    @FXML
    void addCostB() {
        addCost.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("addCost", "");
    }

    @FXML
    void dellCostB() {
        dellCost.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("delCost", "");
    }

    @FXML
    void viewYearB() {
        viewYear.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("viewYear", "");
    }

    @FXML
    void addYearB() {
        addYear.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("addYear", "");
    }

    @FXML
    void dellYearB() {
        dellYear.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("delYear", "");
    }

    @FXML
    void viewUsersB() {
        viewUsers.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("viewClients", "");
    }

    @FXML
    void back() {
        backButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("adminMenu", "");
    }
}
