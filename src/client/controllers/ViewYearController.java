package client.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import client.entityClass.Country;
import client.entityClass.Years;
import client.sample.Client;
import client.sample.ClientInstance;
import client.sceneLoaders.SceneLoaderInstance;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

public class ViewYearController {

    @FXML
    private AnchorPane yearsTable;

    @FXML
    private TableView<Years> yearTable;

    @FXML
    private TableColumn<Years, String> yearColumn;

    @FXML
    private Button backButton;

    public void initialize() {
        fillTableView();
    }

    public void fillTableView() {
        Client client = ClientInstance.INSTANCE.getInstance();
        client.connect();
        ClientInstance.INSTANCE.getInstance().send("getYear");
        ArrayList<String> list = ClientInstance.INSTANCE.getInstance().receiveResultList();
        ObservableList<Years> year = FXCollections.observableArrayList();
        for (int i = 0; i < list.size(); i++) {
           Years years = new Years();
            years.setYear(list.get(i));
            year.add(years);
        }
        yearColumn.setCellValueFactory(new PropertyValueFactory<>("year"));
        yearTable.setItems(year);
    }

    @FXML
    void back() {
        backButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("adminMenu2", "");
    }
}
