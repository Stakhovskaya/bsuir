package client.controllers;

import client.sample.Alert;
import client.sample.Client;
import client.sample.ClientInstance;
import client.sceneLoaders.SceneLoaderInstance;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

import java.util.ArrayList;

public class AddCostController {


    @FXML
    private ChoiceBox<String> productBox;

    @FXML
    private ChoiceBox<String> countryBox;

    @FXML
    private TextField energy;

    @FXML
    private TextField materialcost;

    @FXML
    private TextField salary;

    @FXML
    private TextField anothercosts;

    @FXML
    private TextField cost;


    @FXML
    private Button backButton;
    @FXML
    private Button costbtn;
    @FXML
    private ChoiceBox<String> yearBox;


    @FXML
    void initialize() {
        Client client = ClientInstance.INSTANCE.getInstance();
        client.connect();
        client.send("getProducts");
        ArrayList<String> productsList = client.receiveResultList();
        client.send("getCountry");
        ArrayList<String> countryList = client.receiveResultList();
        client.send("getYear");
        ArrayList<String> yearList = client.receiveResultList();
        ObservableList<String> products = FXCollections.observableArrayList(productsList);
        ObservableList<String> country = FXCollections.observableArrayList(countryList);
        ObservableList<String> year = FXCollections.observableArrayList(yearList);
        productBox.setItems(products);
        countryBox.setItems(country);
        yearBox.setItems(year);
    }
public void setCost(){

}
    public void addcost() {
        cost.setText(String.valueOf(Double.parseDouble(salary.getText()) + Double.parseDouble(energy.getText()) + Double.parseDouble(materialcost.getText()) + Double.parseDouble(anothercosts.getText())));
    }

    public void add() {
        Client client = ClientInstance.INSTANCE.getInstance();
        client.connect();
        if (salary.getText().equals("") || materialcost.getText().equals("") || energy.getText().equals("") || anothercosts.getText().equals("")
                || productBox.getValue() == null || countryBox.getValue() == null || yearBox.getValue() == null) {
            Alert.display("Вы не заполнили поля!");
            return;
        }
        StringBuilder info = new StringBuilder();
        info.append("addCost " + salary.getText() + " " + materialcost.getText() +
                " " + cost.getText() + " " + anothercosts.getText() +
                " " + energy.getText() + " " + countryBox.getValue() + " " + yearBox.getValue() + " " + productBox.getValue());
        Alert.display("Данные добавлены успешно!");
        ClientInstance.INSTANCE.getInstance().send(info.toString());
        if (!ClientInstance.INSTANCE.getInstance().receiveResultBool()) {
            Alert.display("Ошибка добавления");
        }
    }

    @FXML
    void back() {
        backButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("adminMenu2", "");
    }
}
