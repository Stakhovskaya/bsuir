package client.controllers;

import client.entityClass.Product;
import client.sample.Alert;
import client.sample.Client;
import client.sample.ClientInstance;
import client.sceneLoaders.SceneLoaderInstance;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ViewUserCostController {
    @FXML
    private TableView<Product> infoTable;

    @FXML
    private TableColumn<Product, String> productColumn;

    @FXML
    private TableColumn<Product, String> countryColumn;

    @FXML
    private TableColumn<Product, Integer> yearColumn;

    @FXML
    private TableColumn<Product, Double> energyColumn;

    @FXML
    private TableColumn<Product, Double> salaryColumn;

    @FXML
    private TableColumn<Product, Double> materialcostColumn;

    @FXML
    private TableColumn<Product, Double> anothercostColumn;

    @FXML
    private TableColumn<Product, Double> costColumn;

    @FXML
    private Button backButt;

    @FXML
    private TextField search;

    @FXML
    void back(){
        backButt.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("userMenu", "");
    }

    public void initialize() {
        Client client = ClientInstance.INSTANCE.getInstance();
        client.connect();
        fillTableView();
        search.textProperty().addListener(( observable, oldValue,  newValue)->
                filterList(oldValue, newValue));
    }

    public void fillTableView() {
        ClientInstance.INSTANCE.getInstance().send("getUserCost");
        ArrayList<String> list = ClientInstance.INSTANCE.getInstance().receiveResultList();
        ObservableList<Product> products = FXCollections.observableArrayList();
        String[] infoString;
        for (int i = 0; i < list.size(); i++) {
            infoString = list.get(i).split(" ", 9);
            Product product = new Product();
            product.setName(infoString[0]);
            product.setCountry(infoString[1]);
            product.setYear(infoString[2]);
            product.setEnergy(Double.valueOf(infoString[3]));
            product.setSalary(Double.valueOf(infoString[4]));
            product.setMaterialcost(Double.valueOf(infoString[5]));
            product.setAnothercosts(Double.valueOf(infoString[6]));
            product.setCost(Double.valueOf(infoString[7]));
            products.add(product);
        }
        productColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        countryColumn.setCellValueFactory(new PropertyValueFactory<>("country"));
        yearColumn.setCellValueFactory(new PropertyValueFactory<>("year"));
        energyColumn.setCellValueFactory(new PropertyValueFactory<>("energy"));
        salaryColumn.setCellValueFactory(new PropertyValueFactory<>("salary"));
        materialcostColumn.setCellValueFactory(new PropertyValueFactory<>("materialcost"));
        anothercostColumn.setCellValueFactory(new PropertyValueFactory<>("anothercosts"));
        costColumn.setCellValueFactory(new PropertyValueFactory<>("cost"));
        infoTable.setItems(products);
    }
    public void saveToFile() {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Сохранить в файл");
        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            try {
                BufferedWriter outWriter = new BufferedWriter(new FileWriter(file));
                for (Product info : infoTable.getItems()) {
                    outWriter.write(info.toString());
                    outWriter.newLine();
                }
                outWriter.close();
            } catch (IOException e) {
                Alert.display("Ошибка записи в файл!");
            }
        }
    }
    public void filterList(String oldValue, String newValue) {
        ObservableList<Product> filteredList = FXCollections.observableArrayList();
        if (search == null || (newValue.length() < oldValue.length() || newValue == null)) {
            fillTableView();
        } else {
            newValue = newValue.toUpperCase();
            for (Product info : infoTable.getItems()) {
                String filter = info.getName();
                if (filter.toUpperCase().contains(newValue) || filter.toUpperCase().contains(newValue)) {
                    filteredList.add(info);
                }
            }
            infoTable.setItems(filteredList);
        }
    }
}
