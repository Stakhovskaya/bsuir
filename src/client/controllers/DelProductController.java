package client.controllers;

import client.sample.Alert;
import client.sample.ClientInstance;
import client.sceneLoaders.SceneLoaderInstance;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class DelProductController {

    @FXML
    private TextField productField;

    @FXML
    private Button backButton;

    public void del() {
        if (productField.getText().equals("")) {
            Alert.display("Вы не ввели название продукта!");
            return;
        }
        ClientInstance.INSTANCE.getInstance().send(("delProduct " + productField.getText()));
        if (ClientInstance.INSTANCE.getInstance().receiveResultBool()) {
            Alert.display("Данные удалены успешно!");
        }
    }

    @FXML
    void back() {
        backButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("adminMenu", "");
    }
}
