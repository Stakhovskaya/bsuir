package client.controllers;


import client.sceneLoaders.SceneLoaderInstance;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class UserMenuController {

    @FXML
    private Button backButton;

    @FXML
    private Button viewInfoB;

    @FXML
    private Button addInfoB;


    @FXML
    void viewInfo() {
       viewInfoB.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("viewUserCost", "");
    } @FXML
    void addInfo() {
        addInfoB.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("addCostUser", "");
    }

    @FXML
    void back() {
        backButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("sample", "");
    }
}
