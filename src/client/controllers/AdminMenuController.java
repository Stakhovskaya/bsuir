package client.controllers;


import client.sceneLoaders.SceneLoaderInstance;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class AdminMenuController {

    @FXML
    private Button backButton;

    @FXML
    private Button viewCountry;

    @FXML
    private Button addCountry;

    @FXML
    private Button dellCountry;

    @FXML
    private Button viewProd;

    @FXML
    private Button addProd;

    @FXML
    private Button dellProd;

    @FXML
    private Button addAdmin;

    @FXML
    private Button dellAdmin;

    @FXML
    private Button viewAdmin;

    @FXML
    private Button nextButton;

    @FXML
    void viewCountryB() {
        viewCountry.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("viewCountry", "");
    }

    @FXML
    void addCountryB() {
        addCountry.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("addCountry", "");
    }

    @FXML
    void dellCountryB() {
        dellCountry.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("delCountry", "");
    }

    @FXML
    void viewProdB() {
        viewProd.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("viewProduct", "");
    }

    @FXML
    void addProdB() {
        addProd.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("addProduct", "");
    }

    @FXML
    void dellProdB() {
        dellProd.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("delProduct", "");
    }

    @FXML
    void addAdminB() {
        addAdmin.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("addAdmin", "");
    }

    @FXML
    void dellAdminB() {
        dellAdmin.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("dellAdmin", "");
    }

    @FXML
    void viewAdminB() {
        viewAdmin.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("viewAdmin", "");
    }

    @FXML
    void back() {
        backButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("sample", "");
    }

    @FXML
    void next() {
        nextButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("adminMenu2", "");
    }
}
