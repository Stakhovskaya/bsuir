package client.controllers;


import client.sample.Alert;
import client.sample.ClientInstance;
import client.sceneLoaders.SceneLoaderInstance;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class AddCountryController {

    @FXML
    private TextField countryField;

    @FXML
    private Button backButton;


    public void addCountry() {
        if (countryField.getText().equals("")) {
            Alert.display("Вы не ввели название страны!");
            return;
        }
        ClientInstance.INSTANCE.getInstance().send(("addCountry " + countryField.getText()));
        if (ClientInstance.INSTANCE.getInstance().receiveResultBool()) {
            Alert.display("Страна добавлена успешно!");
        }
    }

    @FXML
    void back() {
        backButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("adminMenu", "");
    }
}

