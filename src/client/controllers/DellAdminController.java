package client.controllers;

import client.sample.Alert;
import client.sample.ClientInstance;
import client.sceneLoaders.SceneLoaderInstance;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class DellAdminController {

    @FXML
    private TextField logField;

    @FXML
    private Button backButton;


    public void del() {
        if (logField.getText().equals("")) {
            Alert.display("Вы не ввели логин!");
            return;
        }
        if (logField.getText().equals("admin")) {
            Alert.display("Этого админа нельзя удалить!");
            return;
        }

        ClientInstance.INSTANCE.getInstance().send(("dellAdmin " + logField.getText()));
        if (ClientInstance.INSTANCE.getInstance().receiveResultBool()) {
            Alert.display("Данные удалены успешно!");
        }
    }
    @FXML
    void back() {
        backButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("adminMenu", "");
    }
}