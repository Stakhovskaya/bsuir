package client.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import client.entityClass.Country;
import client.sample.Client;
import client.sample.ClientInstance;
import client.sceneLoaders.SceneLoaderInstance;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

public class ViewCountryController {

    @FXML
    private AnchorPane countriesTable;

    @FXML
    private TableView<Country> countryTable;

    @FXML
    private TableColumn<Country, String> countryColumn;

    @FXML
    private Button backButton;

    public void initialize() {
        fillTableView();
    }

    public void fillTableView() {
        Client client = ClientInstance.INSTANCE.getInstance();
        client.connect();
        ClientInstance.INSTANCE.getInstance().send("getCountry");
        ArrayList<String> list = ClientInstance.INSTANCE.getInstance().receiveResultList();
        ObservableList<Country> countries = FXCollections.observableArrayList();
        for (int i = 0; i < list.size(); i++) {
            Country country = new Country();
            country.setCountry(list.get(i));
            countries.add(country);
        }
        countryColumn.setCellValueFactory(new PropertyValueFactory<>("country"));
        countryTable.setItems(countries);
    }

    @FXML
    void back() {
        backButton.getScene().getWindow().hide();
        SceneLoaderInstance.INSTANCE.getInstance().loadScene("adminMenu", "");
    }


}

