package client.entityClass;

public class Product {
    private  int id_product;
    private String name;
    private String country;
    private  String year;
    private  double energy;
    private  double materialcost;
    private  double salary;
    private  double anothercosts;
    private  double cost;

    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String  getYear() {
        return year;
    }

    public void setYear(String  year) {
        this.year = year;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public double getMaterialcost() {
        return materialcost;
    }

    public void setMaterialcost(double materialcost) {
        this.materialcost = materialcost;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getAnothercosts() {
        return anothercosts;
    }

    public void setAnothercosts(double anothercosts) {
        this.anothercosts = anothercosts;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
